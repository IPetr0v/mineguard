#!/bin/bash

# check inputs and pring usage if need be
if [ -z "$1" ] ; then
	echo "usage: monitor <Process PID>"
	exit
fi

profile_time=2 # only need 1 sample (2 seconds)
output_file=$(pwd)/test.csv

# profile application/miner/VM
./profile.sh $1 $profile_time $output_file

# format data
python format_data.py $output_file ${output_file}_

# get prediction from mineguard. Note: change signature depending upon OS or VM
./../classifier/predict ${output_file}_ $(pwd)/../data/vm-signature.txt

# can add notifications/emails/flagging based on previous prediction here

# remove temp files
rm $output_file
rm ${output_file}_

