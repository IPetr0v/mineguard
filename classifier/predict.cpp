#include "common.h"

using namespace boost;
using namespace std;

// ./predict <test_file> <signature_file> 
#define NUM_ARGS 3

int predict(const char *test_file, const char *signature_file) {
    // we only need one sample for prediction and there are only two
    // labels (0 and 1)
    const size_t num_samples = 1;
    const size_t num_labels = 2;
    const size_t feature_shape[] = {num_samples, num_features};
    const size_t prob_shape[] = {num_samples, num_labels};

    // test and result matrices
    andres::Marray<Feature> features(feature_shape, feature_shape + 2);
    andres::Marray<Probability> probabilities(prob_shape, prob_shape + 2);

    // read test file and create features matrix
    ifstream test_vector(test_file);

    if (!test_vector.is_open()) {
        cout << "Error opening test data\n";
        return 1;
    }

    // for parsing
    string line;
    vector<string> input;

    // skip header
    getline(test_vector, line);

    // load test vector
    getline(test_vector, line);
    split(input, line, is_any_of(","));

    // skip first two elements (application name and time)
    size_t feature = 0;
    for (size_t index = 2; index < input.size(); index++, feature++) {
        features(0, feature) = atol(input[index].c_str());
    }

    // deserialize "signatures" from signature file
    andres::ml::DecisionForest<Feature, Label, Probability> decision_forest;
    ifstream signatures(signature_file);
    decision_forest.deserialize(signatures);

    // get prediction - probabilities(0, 0) is probability of the label being
    // *zero*
    decision_forest.predict(features, probabilities);
    bool is_miner = (probabilities(0, 0) < 0.5);

    // output result to stdout for now but can be easily modified later on to
    // notify sysadmin, etc.
    if (is_miner)
        cout << "Mining detected! Run for your lives!" << endl;
    else
        cout << "Relax, no one's mining." << endl;

    return 0;
}

int main(int argc, char **argv) {
    if (argc != NUM_ARGS) {
        cout << "usage: ./predict <test_file> <signature_file>\n";
        return EXIT_FAILURE;
    }

    char *test_file = argv[1];
    char *signature_file = argv[2];
    return predict(test_file, signature_file);
}

